﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Task1
{
    [Serializable]
    [XmlType("library")]
    public class Library : ICountingBooks
    {
        [XmlAttribute("name")]
        public string LibraryName { get; set; }
        [XmlArray("sections")]
        public List<Section> SectionsList { get; set; }
        [XmlIgnore]
        public List<Author> AllAuthorsList { get; private set; }

        public Library()
        { }

        public Library(string name)
        {
            LibraryName = name;
            SectionsList = new List<Section>();
            AllAuthorsList = new List<Author>();
        }

        public void Serializable(string xmlFileName)
        {
           XmlSerializer serializer = new XmlSerializer(typeof(Library));

            using (FileStream fs = new FileStream(xmlFileName, FileMode.OpenOrCreate))
            {
                serializer.Serialize(fs, this);
            }
        }

        public void Deserializable(string xmlFileName)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Library));
            TextReader reader = new StreamReader(xmlFileName);
            object obj = deserializer.Deserialize(reader);
            Library lib = obj as Library;
            this.LibraryName = lib.LibraryName;
            this.SectionsList = lib.SectionsList;
            AllAuthorsList = new List<Author>();
            reader.Close();
        }

        public void AddSection(string name)
        {
            SectionsList.Add(new Section(name,this));
        }

        public int GetBooksCount()
        {
            int count = 0;
            foreach (var section in SectionsList)
            {
                count += section.GetBooksCount();
            }
            return count;
        }

        public Section GetSectionByName(string name)
        {
            return SectionsList.FirstOrDefault(elem => elem.SectionName.ToLower() == name.ToLower());
        }

        public Author GetAuthorWithMaxBooks()
        {
            return AllAuthorsList.Max();
        }

        public Section GetSectionWithMaxBooks()
        {
            return SectionsList.Max();
        }

        public Book GetBookWithMinPages()
        {
            List<Book> allBooks = new List<Book>();
            foreach (var section in SectionsList)
            {
                allBooks = allBooks.Concat(section.SectionBooksList).ToList();
            }
            return allBooks.Min();
        }

        public void DeleteAllAuthorBooks(string name)
        {
            foreach (var section in SectionsList)
            {
                section.SectionBooksList.RemoveAll((item => item.BookAuthor.Name == name));
            }
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder(LibraryName).Append("\n");
            foreach (var section in SectionsList)
            {
                str.Append(section);
            }
            return str.ToString();
        }
    }
}