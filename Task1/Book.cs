﻿using System;
using System.Xml.Serialization;

namespace Task1
{
    [Serializable]
    [XmlType("book")]
    public class Book : IComparable
    {
        [XmlElement("title")]
        public string Title { get; set; }
        [XmlElement("pages")]
        public int PagesCount { get; set; }
        [XmlElement("author")]
        public Author BookAuthor { get; set; }

        private Section CurrentSection { get; set; }

        public Book()
        { }

        public Book(string title, int pages, Author author, Section section)
        {
            Title = title;
            PagesCount = pages;
            BookAuthor = author;
            CurrentSection = section;
        }

        public override string ToString()
        {
            return $"Title: {Title}, author: {BookAuthor}, num of pages: {PagesCount}";
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is Book otherBook)
                return PagesCount.CompareTo(otherBook.PagesCount);
            else
                throw new ArgumentException("Object is not a Book");
        }
    }
}
