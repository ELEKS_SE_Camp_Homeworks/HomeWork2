﻿

namespace Task1
{
    public interface ICountingBooks
    {
        int GetBooksCount();
    }
}
