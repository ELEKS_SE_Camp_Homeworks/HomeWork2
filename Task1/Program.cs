﻿using System;

namespace Task1
{
    public class Program
    {
        static void Main(string[] args)
        {
            Library library = new Library("Politech");

            XmlWork xmlWrk = new XmlWork(library, "library.xml");
            xmlWrk.ReadLibraryDataFromXML();
            //library.AddSection("IT");
            //library.AddSection("UkrLt");
            //library.AddSection("ForegnLit");
            //library.GetSectionByName("IT").AddBook("FFF", 390, new Author("Oleh Melnyk"));
            //library.GetSectionByName("UkrLt").AddBook("YYY", 390, new Author("Oleh Melnyk"));
            //library.GetSectionByName("ForegnLit").AddBook("AAA", 390, new Author("Oleh Melnyk"));
            //Console.WriteLine(library);
            library.Serializable("serialized.xml");
            
            Console.WriteLine(library);
            //Console.WriteLine(library.GetAuthorWithMaxBooks());
            //Console.WriteLine(library.GetBookWithMinPages());
            //Console.WriteLine(library.GetSectionWithMaxBooks());

            Console.ReadLine();
        }
    }
}
