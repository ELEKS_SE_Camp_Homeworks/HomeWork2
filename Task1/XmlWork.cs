﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Task1
{
    public class XmlWork
    {
        private XDocument XDoc;
        private Library CurrentLibrary { get; set; }
        public string CurrXmlFile { get; set; }

        public XmlWork(Library library, string docName)
        {
            CurrXmlFile = docName;
            CurrentLibrary = library;
        }

        public void ReadLibraryDataFromXML()
        {
            XDoc = XDocument.Load(CurrXmlFile);
            string authorName = "";
            string bookTitle = "";
            int pagesNum = 0;
            if (XDoc != null && XDoc.Root.HasElements)
            {
                XElement libraryElem = XDoc.Element("library");
                if (libraryElem != null && libraryElem.HasElements)
                {
                    foreach (var booksNode in libraryElem.Elements("books"))
                    {
                        var attr = booksNode.Attribute("section");
                        if (attr != null)
                        {
                            Section currentSection = CurrentLibrary.GetSectionByName(attr.Value);
                            if (currentSection == null) CurrentLibrary.AddSection(attr.Value);
                            foreach (var book in booksNode.Elements())
                            {
                                authorName = book.Attribute("author").Value;
                                bookTitle = book.Attribute("title").Value;
                                pagesNum = int.Parse(book.Attribute("pages").Value);
                                CurrentLibrary.GetSectionByName(attr.Value).AddBook(bookTitle,pagesNum,new Author(authorName));
                            }
                        }
                    }
                }
            }
        }

        public void SaveLibraryDataToXML(string filename)
        {
            XElement xRoot = new XElement("library");
            foreach (var section in CurrentLibrary.SectionsList)
            {
                XElement booksElem = new XElement("books", new XAttribute("section", section.SectionName));
                xRoot.Add(booksElem);
                foreach (var book in section.SectionBooksList)
                {
                    XElement bookElem = new XElement("book",
                        new XAttribute("title", book.Title),
                        new XAttribute("author", book.BookAuthor.Name),
                        new XAttribute("pages", book.PagesCount.ToString()));
                    booksElem.Add(bookElem);
                }
            }
            var newXdoc = new XDocument(xRoot);
            newXdoc.Save(filename);
        }

        public void AddBookNode(Book newBook, string sectionName)
        {
            XDoc = XDocument.Load(CurrXmlFile);
            if (XDoc != null && XDoc.Root.HasElements)
            {
                XElement xRoot = XDoc.Element("library");
                if (XDoc != null && XDoc.Root.HasElements)
                {
                    foreach (var xnode in xRoot.Elements())
                    {
                        var attr = xnode.Attribute("section");
                        if (attr != null && attr.Value == sectionName.ToLower())
                        {
                            XElement bookElem = new XElement("book");
                            bookElem.SetAttributeValue("title", newBook.Title);
                            bookElem.SetAttributeValue("author", newBook.BookAuthor.Name);
                            bookElem.SetAttributeValue("pages", newBook.PagesCount);
                            xnode.Add(bookElem);
                            CurrentLibrary.GetSectionByName(sectionName).AddBook(newBook.Title,newBook.PagesCount,newBook.BookAuthor);
                            break;
                        }
                    }
                }
            }
            XDoc.Save(CurrXmlFile);
        }

        public void AddSectionNode(string sectionName)
        {
            XDoc = XDocument.Load(CurrXmlFile);
            if (XDoc != null && XDoc.Root.HasElements)
            {
                XElement xRoot = XDoc.Element("library");
                XElement sectionElement = new XElement("books");
                sectionElement.SetAttributeValue("section", sectionName.ToLower());
                xRoot.Add(sectionElement);
                CurrentLibrary.AddSection(sectionName.ToLower());
                XDoc.Save(CurrXmlFile);
            }
        }

        public void DeleteNodeByTitle(string title)
        {
            XDoc = XDocument.Load(CurrXmlFile);
            if (XDoc != null && XDoc.Root.HasElements)
            {
                XElement libraryElem = XDoc.Element("library");
                if (libraryElem != null && libraryElem.HasElements)
                {
                    foreach (var booksElem in libraryElem.Elements("books"))
                    {
                        booksElem.Elements("book").Where(t => t.Attribute("title").Value == title.ToLower()).Remove();
                        CurrentLibrary.GetSectionByName(booksElem.Attribute("name").Value).DeleteBook("title");
                        XDoc.Save(CurrXmlFile);
                    }
                }
            }
        }
    }
}
