﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Task1
{
    [Serializable]
    [XmlType("section")]
    public class Section : ICountingBooks, IComparable
    {
        [XmlAttribute("name")]
        public string SectionName { get; set; }
        [XmlArray("books")]
        public List<Book> SectionBooksList { get; set; }

        private Library CurrentLibrary { get; set; }

        public Section()
        { }

        public Section(string name,Library currentLibrary)
        {
            SectionBooksList = new List<Book>();
            SectionName = name;
            CurrentLibrary = currentLibrary;
        }

        public int GetBooksCount()
        {
            return SectionBooksList.Count;
        }

        public void AddBook(string title, int pages, Author author)
        {
            Author existAuthor = CurrentLibrary.AllAuthorsList.FirstOrDefault(elem => elem.Name.ToLower() == author.Name.ToLower());
            if (existAuthor != null)
            {
                SectionBooksList.Add(new Book(title, pages, existAuthor, this));
                existAuthor.AddAuthorBook(SectionBooksList[GetBooksCount() - 1]);
            }
            else
            {
                SectionBooksList.Add(new Book(title, pages, author, this));
                CurrentLibrary.AllAuthorsList.Add(author);
                author.AddAuthorBook(SectionBooksList[GetBooksCount() - 1]);
            }
        }

        public void DeleteBook(string title)
        {
            SectionBooksList.RemoveAll(item => item.Title == title);
        }

        public void SortUpByPages()
        {
            SectionBooksList = SectionBooksList.OrderBy(item => item).ToList();
        }

        public void SortDownByPages()
        {
            SectionBooksList = SectionBooksList.OrderByDescending(item => item).ToList();
        }

        public List<Book> SearchBookByTitle(string searchTitle)
        {
            return SectionBooksList.Where(item => item.Title.ToLower() == searchTitle.ToLower()).ToList();
        }

        public List<Book> SearchBookByAuthor(string authorName)
        {
            return SectionBooksList.Where(item => item.BookAuthor.Name.ToLower() == authorName.ToLower()).ToList();
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder($"Section: {SectionName}\n");
            foreach (var book in SectionBooksList)
            {
                str.Append(book).Append("\n");
            }
            return str.ToString();
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is Section otherSection)
                return GetBooksCount().CompareTo(otherSection.GetBooksCount());
            else
                throw new ArgumentException("Object is not a Section");
        }
    }
}
