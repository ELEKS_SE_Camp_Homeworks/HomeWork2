﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Task1
{
    [Serializable]
    [XmlType("author")]
    public class Author : ICountingBooks,IComparable
    {
        [XmlElement("name")]
        public string Name { get; set; }
        private List<Book> AuthorBooksList { get; set; }

        public Author()
        {}

        public Author(string name)
        {
            Name = name;
            AuthorBooksList = new List<Book>();
        }

        public void AddAuthorBook(Book book)
        {
            AuthorBooksList.Add(book);
        }

        public int GetBooksCount()
        {
            return AuthorBooksList.Count;
        }

        public void PrintBooks()
        {
            foreach (var book in AuthorBooksList)
            {
                Console.WriteLine(book);
            }
        }

        public override string ToString()
        {
            return $"{Name}";
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            if (obj is Author otherAuthor)
                return GetBooksCount().CompareTo(otherAuthor.GetBooksCount());
            else
                throw new ArgumentException("Object is not a Author");
        }
    }
}
